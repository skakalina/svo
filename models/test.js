const {Schema, mongoose} = require('../database');

let TestSchema = new Schema(
    {
        first_name:{type:String,},
        last_name:{type:String},
    }
);
module.exports = mongoose.model('Test', TestSchema);

