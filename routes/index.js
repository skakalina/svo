const express = require('express');
const router = express.Router();
const Test = require('../models/test');
const multer = require('multer');
const fs = require('fs');
const storage =  multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    },
    fileFilter: function (req, file, cb) {
        if (file.mimetype !== 'image/png') {
            req.fileValidationError = 'goes wrong on the mimetype';
            return cb(null, false, new Error('goes wrong on the mimetype'));
        }
        cb(null, true);
    }

});


const upload = multer({ storage: storage });

/* GET home page. */
router.get('/', function(req, res) {
  res.render('test');
});


router.post('/', upload.single('ava'), function(req, res) {
    if (req.file.mimetype !== 'image/jpeg'){
        fs.unlink(req.file.path, (err)=> {
            if (err) {
                console.log("failed to delete local image:"+err);
            } else {
                console.log('successfully deleted local image');
            }
        })
    }
    Test.create(
        {
            first_name: req.body.first_name,
            last_name:req.body.last_name
        }
    ).then(function (result,err) {
        if (err){
            res.render('test', { test: '', errors: err});
        } else {
            res.send('Принято');
        }
    });
});

module.exports = router;