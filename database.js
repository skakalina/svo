//подключение к mongoose и базе данных
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test1', { autoIndex: false }); //{ autoIndex: false } перестает индексировать что улучшает производительность

var Schema = mongoose.Schema;

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

module.exports.Schema = Schema;
module.exports.mongoose = mongoose;